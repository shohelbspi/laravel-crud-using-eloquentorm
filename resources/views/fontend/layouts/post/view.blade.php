@extends('fontend/layouts/master')

@section('title')
    Post Single View Page
@endsection

@section('Bodycontent')

<div class="container my-4">
    <div class="card" style="width: 50%">
        <div class="card-header">
            <a href="{{url ('index')}}"><button style="float: right" class="btn btn-success">All Post</button></a>
            <h2>Single Post View</h2>
        </div>
        <div class="card-body">
            <p>Title  : {{$viewPost->title}}</p>
            <p>Body   : {{$viewPost->body}}</p>
        </div>
    </div>
</div>

@endsection 