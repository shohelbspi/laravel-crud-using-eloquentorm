@extends('fontend/layouts/master')

@section('title')
    Post Edit Page
@endsection

@section('Bodycontent')

    <div class="container my-4 " style="width: 60%">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <a style="float: right"  href="{{ url ('index')}}"><button type="submit" class="btn btn-dark">All Post</button></a>
                        <h1 style="text-align: center">All Post</h1>
                    </div>
                    <div class="card-body">
                        <form method="POST"}} action="{{url ('update/'.$editPost->id )}}">
                            @csrf
                            <div class="mb-3">
                                <label for="title" class="form-label">Post Title</label>
                                <input type="text" class="form-control" value="{{$editPost->title}}" id="title" name="post_title">
                            </div>
                            <div class="mb-3">
                                <label for="body" class="form-label">Post Body</label>
                                <input type="text" class="form-control" value="{{$editPost->body}}" id="body" name="post_body">
                            </div>
                        
                            <button type="submit" class="btn btn-primary">Update Post</button>
                        </form>
                    </div>
                </div>
            </div>
       
        </div>
    </div>

@endsection
