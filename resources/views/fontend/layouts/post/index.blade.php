@extends('fontend/layouts/master')


@section('title')
    Post Index page

@endsection

@section('Bodycontent')
    <div class="container my-4" style="width: 80%">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <a style="float: right"  href="{{ url ('create')}}"><button type="submit" class="btn btn-dark">Create Post</button></a>
                        <h1 style="text-align: center">All Post</h1>
                    </div>

                    <div class="card-body">
                        <table class="table ">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($post as $item)
                                <tr>
                                    <th>{{$item->id}}</th>
                                    <td>{{$item->title}}</td>
                                    <td>{{Str::words($item->body,8)}}</td>
                              
                
                                    <td>
                                        {{-- view button --}}
                                        <a  href="{{ url ('view/'.$item->id)}}"><button type="submit" class="btn btn-success">View</button></a>
                                        {{-- edit button --}}
                                        <a  href="{{ url ('edit/'.$item->id)}}"><button type="submit" class="btn btn-primary">Edit</button></a>
                                        {{-- delete button --}}
                
                                        <a  href="{{ url ('delete/'.$item->id)}}"><button type="submit" class="btn btn-danger">Delete</button></a>
                
                                    </td>
                              
                                </tr>
                
                                @endforeach
                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
