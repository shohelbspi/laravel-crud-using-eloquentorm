<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\studentController;
use App\Http\Controllers\postController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/stuindex', [studentController::class, 'index']);

Route::get('/index', [postController::class, 'index'])->name('post_index');
Route::get('/create', [postController::class, 'create'])->name('post_create');
Route::post('/store', [postController::class, 'store'])->name('post_store');
Route::get('/view/{id}', [postController::class, 'view'])->name('post_view');
Route::get('/edit/{id}', [postController::class, 'edit'])->name('post_edit');
Route::post('/update/{id}', [postController::class, 'update'])->name('post_update');
Route::get('/delete/{id}', [postController::class, 'delete'])->name('post_delete');
