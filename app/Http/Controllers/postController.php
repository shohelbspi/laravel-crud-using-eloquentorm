<?php

namespace App\Http\Controllers;
use\App\Models\Post;

use Illuminate\Http\Request;

class postController extends Controller
{
    public function index()
    {
        $post = new Post();
        $post  = $post::all();
        return view('fontend/layouts/post/index', compact('post'));
    }

    public function create()
    {
        return view('fontend/layouts/post/create');

    }

    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->post_title;
        $post->body  = $request->post_body;
        $post->save();
        return redirect()->route('post_index');
    
    }

    public function view($id)
    {
        $viewPost = Post::where('id',$id)->first();
        return view('fontend/layouts/post/view', compact('viewPost'));
    }

    public function edit($id)
    {
        $editPost = Post::find($id);
        return view('fontend/layouts/post/edit', compact('editPost'));
    }

    public function update(Request $request)
    {
        $update = Post::find($request->id);
        $update->title = $request->post_title;
        $update->body  = $request->post_body;
        $update->save();
        return redirect()->route('post_index');

    }


    public function delete($id)
    {
        Post::where('id', $id)->delete();
        return redirect()->route('post_index');

    }
   
}
