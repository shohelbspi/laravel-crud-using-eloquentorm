<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use\App\Models\Student;

class studentController extends Controller
{
    public function index(){
        $students = Student::whereBetween('id', [3,30])->orderby('id', 'DESC')->get();
        return $students;
    }
}
